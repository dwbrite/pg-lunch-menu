package main

import (
	"net/http"
	"github.com/PuerkitoBio/goquery"
	"encoding/json"
	"regexp"
)

type Product struct {
	ProductName	string
	Price       string
	Description string
	Allergens	string
}

type DailySpecials struct {
	Day string
	Specials []Product
}

func parseSpecials(doc *goquery.Document) []DailySpecials {
	var dailySpecialsList []DailySpecials

	// For each daily special section
	doc.Find(".foodCarousel--day").Each(func(i int, specialsOfTheDay *goquery.Selection) {
		// Create a DailySpecial object with the day and an empty specials array
		day := specialsOfTheDay.Find(".dayheadline").Text()
		dailySpecialItem := DailySpecials{
			day,
			[]Product{},
		}

		// For each special
		specialsOfTheDay.Find(".woocommerce").Each(func(i int, section *goquery.Selection) {
			// Create the product and add it to the specials array
			product := Product {
				section.Find(".cobie-product-name").Text(),
				section.Find(".price").Text(),
				section.Find(".cobie-product-description").Text(),
				section.Find("sup").Text(),
			}
			dailySpecialItem.Specials = append(dailySpecialItem.Specials, product)
		})

		// And add it to the list of daily specials
		dailySpecialsList = append(dailySpecialsList, dailySpecialItem)
	})

	return dailySpecialsList
}

func getMenuPage() (*goquery.Document, error) {
	// Grab contents from the website
	res, err := http.Get("http://www.cobie.de/speisekarte")
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// Create a goquery document from the body contents
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, err
	}

	return doc, nil
}

func serveMenu(writer http.ResponseWriter, request *http.Request) {
	doc, err := getMenuPage()
	if err != nil {
		writer.WriteHeader(500)
		writer.Write([]byte("500 internal server error:\n\t" + err.Error() + "\n"))
		return
	}

	dailySpecialsList := parseSpecials(doc)
	if len(dailySpecialsList) < 1 {
		writer.WriteHeader(500)
		writer.Write([]byte("500 internal server error:\n\thttp://www.cobie.de/speisekarte appears to have changed.\n"))
		return
	}

	var responseBody interface{} = nil

	path := request.URL.Path

	menuPath := regexp.MustCompile("^/menu/?$")
	todayPath := regexp.MustCompile("^/menu/(today|heute)/?$")
	tomorrowPath := regexp.MustCompile("^/menu/(tomorrow|morgen)/?$")
	uebermorgenPath := regexp.MustCompile("^/menu/(after-tomorrow|uebermorgen)/?$")

	switch {
	case menuPath.MatchString(path): responseBody = dailySpecialsList
	case todayPath.MatchString(path): responseBody = dailySpecialsList[0]
	case tomorrowPath.MatchString(path):
		if len(dailySpecialsList) < 2 {
			writer.WriteHeader(404)
			writer.Write([]byte("404 resource not found - cobie might not be open that day\n"))
			return
		} else {
			responseBody = dailySpecialsList[1]
		}
	case uebermorgenPath.MatchString(path):
		if len(dailySpecialsList) < 3 {
			writer.WriteHeader(404)
			writer.Write([]byte("404 resource not found - cobie might not be open that day\n"))
			return
		} else {
			responseBody = dailySpecialsList[1]
		}
	default: writer.WriteHeader(404); writer.Write([]byte("404 page not found\n")); return
	}

	json.NewEncoder(writer).Encode(&responseBody)
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", serveMenu)
	http.ListenAndServe(":44441", mux)
}
